FROM node:17.4.0


COPY package.json .

ADD . .
RUN npm install
CMD ["npm", "run", "serveAWS"]
EXPOSE 6060
