# FRONT-END
- Vue Js 3 @vue/cli 5.0.1
- Node.js v17.4.0.

## Link
[1. Dependency](#dependency)

[2. System 구성](#system)

[3. LocalBuild](#localBuild)

[4. CI/CD](#cicd)

[5. Release](#release)

## Dependency <a id=dependency></a>
- package.json 참조
- @sipec/vue3-tags-input
- axios
- bootstrap
- mosha-vue-toastify
- simple-code-editor
- sockjs-client
- vue
- vue-router
- vue3-markdown
- vue3-markdown-it
- vue3-tour
- webstomp-client


## System 구성 <a id=system></a>
![System 구성.JPG](./System 구성.JPG)

## LocalBuild <a id=localBuild></a>

- package.json 참조

npm run [Mode]

|Mode | Description|
| ----| ---- |
|serve| localhost 로 접근|
|Sbuk| 별도 개발 환경일 경우 .env.sbuk 파일 수정하여 해당 IP로 접근|
|Home| 자주 쓰는 개발 환경일 경우 .env.home 파일 수정하여 해당 IP로 접근|

## CI/CD <a id=cicd></a>

- JenkinsFile 참조
- GitLab PR Merge 시 Jenkins Server(Local PC)에서 Trigger를 활성화하여 Jenkin File 실행
- Jenkins File 구성 

    1. 기존 Docker Image 삭제
    2. DockerFile의 구성으로 Docker Image 생성
    3. 만들어진 Docker Image로 Container 실행

## Release <a id=release></a>

- 2022/03 : ChatService 개발
- 2022/04 : Question Service 개발
- 2022/05 : Posting Service 개발
- 2022/07 : PostService 중 list Click 시 저장된 데화 정보 불러오도록 수정

