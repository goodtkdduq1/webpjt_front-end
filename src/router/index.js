import { createWebHistory, createRouter } from "vue-router";
import FirstPage from '@/components/firstPage/firstPage.vue'
import SecondPage from '@/components/secondPage/SecondPage.vue'
import ThirdPage from '@/components/ThridPage/ThirdPage.vue'
import FourthPage from "@/components/fourthPage/FourthPage";
import ContentsList from "@/components/fourthPage/components/ContentsList";
import MarkDownViewer from "@/components/fourthPage/components/MarkDownViewer";
import WriteContents from "@/components/fourthPage/components/WriteContents";
const routes= [
  {
    path: '/',
    redirect: '/chat', // 초기 진입 페이지 설정
  },
  {
    path: '/SecondPage',
    name: 'SecondPage',
    component: SecondPage
  },
  {
    path:'/chat',
    name:'firstPage',
    component : FirstPage,
    props: true
  },
  {
    path:'/Posting',
    name:'ThirdPage',
    component : ThirdPage,
    props: true
  },
  {
    path:'/PrivatePosting',
    name:'FourthPage',
    component : FourthPage,
    props: true,
    children: [
      {
        path:'contentsList',
        name:'ContentsList',
        component : ContentsList,
        props: true
      },
      {
        path:'content',
        name:'MarkDownViewer',
        component : MarkDownViewer,
        props: true
      },
      {
        path:'newContent',
        name:'WriteContents',
        component : WriteContents,
        props: true
      },
    ]
  },

]

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
