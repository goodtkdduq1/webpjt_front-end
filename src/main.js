import { createApp } from 'vue'
import App from './App.vue'
import router from './router'                   // router 추가
import "bootstrap/dist/css/bootstrap.min.css";  // bootstrap 추가
import "bootstrap";                             // bootstrap 추가
import axios from "axios";                      // axios 추가
import Vue3Tour from 'vue3-tour'
import 'vue3-tour/dist/vue3-tour.css'
import Vue3Storage from "vue3-storage";
import { store } from "./store";
import PrimeVue from 'primevue/config';
import Aura from '@primevue/themes/aura';
import 'primeicons/primeicons.css'
//import MdEditorV3 from 'md-editor-v3'

const app = createApp(App)
app.config.globalProperties.axios = axios;
app.config.globalProperties.mainServerIP = process.env.VUE_APP_FRIST_IP;
app.config.globalProperties.secondServerIP=process.env.VUE_APP_SECOND_IP;
app.config.globalProperties.thirdServerIP=process.env.VUE_APP_THIRD_IP;
app.config.globalProperties.$store = store;

app.use(PrimeVue, {
    theme: {
        preset: Aura,
        options:{
            darkModeSelector:false || 'none'
        }
    }
});
app.use(Vue3Tour)
app.use(Vue3Storage)
app.use(store)
//app.use(MdEditorV3.MdEditor)
app.use(router).mount('#app')


