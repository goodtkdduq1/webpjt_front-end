import { createStore } from 'vuex'

export const store = createStore ({
  state: {
    userName:""
  },
  getters: {
    getUserName: state => {
      return state.userName;
    }
  },
  mutations: {
    setUserName(state,name)
    {
      state.userName=name;
    }
  },
  actions: {
  },
})